#Google Map iOS 学习笔记
---
##iOS SDK 安装与集成
###一、开发环境
iOS 6.0, Xcode 5.0以上。<br/>
CocoaPods 2.9

###二、安装
在Podfile内加入

```
pod 'Google-Maps-iOS-SDK', '~> 1.6.1'
```

###三、集成
1. 首先到Google申请使用API的权限，并取回secret码，打开以下地址：<https://cloud.google.com/console>。注意，在申请时，bundle的名称和路径必须与你的项目设定一致，否则不能正确执行。
2. 在AppDelegate类中加入以下代码：

```
[GMSServices provideAPIKey:@"{your secret code}"];
```
整体来说，集成还是相当简单的。

##使用Google Map iOS版
###一、基本使用：
1. 在xib中放置任意大小的view，将其类名改成：`GMSMapView`。
2. 将这个mapView设置为IBOutlet到controller的代码中，然后就可以设置其属性和行为了，下面是一个基本的使用：

```
@implementation AMMainViewController
{
    __weak GMSMapView *_mapView;
    __strong CLLocationManager *_locationManager;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _mapView = (GMSMapView *)self.view;
    _mapView.myLocationEnabled = YES;
    _mapView.camera = [GMSCameraPosition cameraWithLatitude:0
                                                  longitude:0
                                                       zoom:15];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    _locationManager.distanceFilter = 50; // meters
    [_locationManager startUpdatingLocation];
}


- (void)updateMapWithLocation:(CLLocation *)location
{
    GMSCameraPosition *prevCamera = _mapView.camera;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude
                                                                 zoom:prevCamera.zoom];
    _mapView.camera = camera;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = location.coordinate;
    marker.title = @"Hello, Map.";
    marker.snippet = @"China";
    marker.map = _mapView;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0)
    {
        [self updateMapWithLocation:location];
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Load location error: %@", [error localizedDescription]);
}

```
以上代码算是集成了CLLocationManager的一个使用，CLLocationManager每隔50米刷新一次，然后在mapView上加一个marker。具体的代码说明及每一个类的作用，后面将会讲到。

###二、基本设置
####1、Map Types 地图显示类型
|Type |	Value |	Description|
| ------------ | ------------- | ------------ |
|Normal |	kGMSTypeNormal |	Typical road map. Roads, some man-made features, and important natural features such as rivers are shown. Road and feature labels are also visible. This is the default map mode in Google Maps for iOS.|
|Hybrid	 | kGMSTypeHybrid |	Satellite photograph data with road maps added. Road and feature labels are also visible. This map type can be enabled on the Google Maps app for iOS by turning on the Satellite view.|
|Satellite	| kGMSTypeSatellite	Satellite | photograph data. Road and feature labels are not visible. This mode is not available in Google Maps for iOS. |
|Terrain	| kGMSTypeTerrain	| Topographic data. The map includes colors, contour lines and labels, and perspective shading. Some roads and labels are also visible.|
|None |	kGMSTypeNone|	No map tiles. The base map tiles will not be rendered. This mode is useful in conjunction with tile layers. The display of traffic data will be disabled when the map type is set to none.|

####2、显示指南针按钮
```
mapView.settings.compassButton = YES;
```

####3、显示我的位置按钮
```
mapView.settings.myLocationButton = YES;
```

####4、禁用手势
|Gestures|Description|
|---|---|
|scrollGestures | controls whether scroll gestures are enabled or disabled. If enabled, users may swipe to pan the camera.|
|zoomGestures | controls whether zoom gestures are enabled or disabled. If enabled, users may double tap, two-finger tap, or pinch to zoom the camera. Note that double tapping may pan the camera to the specified point.|
|tiltGestures | controls whether tilt gestures are enabled or disabled. If enabled, users may use a two-finger vertical down or up swipe to tilt the camera.|
|rotateGestures | controls whether rotate gestures are enabled or disabled. If enabled, users may use a two-finger rotate gesture to rotate the camera.|

###三、Camera的使用
`camera`表示一个视角，你可以理解为你想显示以多大多小在哪儿的地图。`camera`有经度、纬度及zoom的设定，定义了视角中心的经纬度和显示比例，将一个`camera`赋给`mapView`时，`mapView`就会根据`camera`的定义显示相应的地图，而从一个`mapView`取得`camera`时，则是取得当前`mapView`显示的这块地图的中心点及缩放比例的相应信息，注意了，是当前，如果你移动过了地图或改变了缩放大小，那么`camera`也会相应的改变的。

下面是示例代码：

```
GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude
                                                                 zoom:prevCamera.zoom];
_mapView.camera = camera;

// or
[_mapView animateToLocation:location.coordinate];
```

###四、Marker的使用
`marker`是在地图上加标记。它有`标题`和`注释`，用于定位的经纬度。

####1、添加、删除Marker
添加marker:

```
CLLocationCoordinate2D position = CLLocationCoordinate2DMake(0, 0);
GMSMarker *marker = [GMSMarker markerWithPosition:position];
marker.title = @"Hello World";
marker.map = mapView_;
```

删除某个marker:

```
marker.map = nil;
```

添除地图上所有的marker:

```
[mapView clear];
```

####2、自定义Marker
更改marker的颜色：

```
marker.icon = [GMSMarker markerImageWithColor:[UIColor blackColor]];
```

更改marker的图标：

```
CLLocationCoordinate2D position = CLLocationCoordinate2DMake(51.5, -0.127);
GMSMarker *london = [GMSMarker markerWithPosition:position];
london.title = @"London";
london.icon = [UIImage imageNamed:@"house"];
london.map = mapView_;
```

更改marker的透明度：

```
marker.opacity = 0.6;
```

更改marker的InfoWindow显示信息：

```
marker.title = @"show me the money";
marker.snippet = @"StarCraft game start!!!!!!";
```

###五、Polylines的使用
`polylines`拆线，就是在地图上画线，这个东东相当有用。

####1、创建折线
一般创建流程是：

1. Create a `GMSMutablePath` object.
2. Set the points in the path with the `addCoordinate:` or `addLatitude:longitude:` methods.
3. Instantiate a new `GMSPolyline` object using the path as an argument.
4. Set other properties, such as `strokeWidth` and `strokeColor`, as desired.
5. Set the `map` property of the `GMSPolyline`.
6. The polyline appears on the map.

下面是示例代码：

```
GMSMutablePath *path = [GMSMutablePath path];
[path addCoordinate:CLLocationCoordinate2DMake(37.36, -122.0)];
[path addCoordinate:CLLocationCoordinate2DMake(37.45, -122.0)];
[path addCoordinate:CLLocationCoordinate2DMake(37.45, -122.2)];
[path addCoordinate:CLLocationCoordinate2DMake(37.36, -122.2)];
[path addCoordinate:CLLocationCoordinate2DMake(37.36, -122.0)];

GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
rectangle.map = mapView;
```

`GMSMutablePath`类是用来表示一条要绘制的路径，它用于表示 **数据** 部份，用它来添加折线所要经过的所有关键点。而`GMSPolyline`类则是用来表示`mapview`上要显示出来的折线，它是UI显示部份，用于设置拆线的外观，如折线的粗线及颜色。

####2、删除拆线
删除某条折线：

```
polyline.map = nil;
```

删除地图上所有的拆线：

```
[mapView clear]
```


##使用Google Map API
主要是路线部份的查询，别的功能暂时用不上，主要看路线部份的`Google Directions API`吧。

###Google Directions API
####1、简介
Google Directions API 是一种使用 HTTP 请求计算多个位置间路线的服务。您可以搜索公交、行车、步行或骑车等多种交通方式的路线。您可以通过文本字符串（例如，“伊利诺斯州芝加哥市”或“澳大利亚新南威尔士州达尔文市”）或纬度/经度坐标的形式来指定路线的起点、目的地和路标。Google Directions API 可以使用一系列路标返回多段路线。

此服务通常适用于计算路线的静态（事先已知）地址，以便将应用内容放置在地图上；但此服务不适用于对用户输入的内容进行实时响应。有关动态路线计算（例如，在用户界面元素中），请参阅 JavaScript API V3 路线服务文档。

>计算路线是一项既耗时又耗资源的任务。请尽可能使用此处介绍的服务提前计算已知地址，并将结果存储在您自己设计的临时缓存中。

####2、路线请求
Directions API 的请求采用以下形式：

```
http://maps.googleapis.com/maps/api/directions/output?parameters
```

其中，output 可以是以下两个值中的任意一个：

|名称|说明|
|--|--|
|json（推荐）|用于表示以 JavaScript 对象表示法 (JSON) 的形式输出|
|xml|用于表示以 XML 的形式输出|


####3、请求参数
**必填参数**

| 参数名 |说明 |
|---|---|
|origin|要根据其计算路线的地址或文本纬度/经度值。如果您将地址作为字符串传递，那么路线服务会对该字符串进行地理编码，然后将其转换为纬度/经度坐标以计算路线。如果您传递的是坐标值，请确保纬度值与经度值之间无空格。|
|destination|要根据其计算路线的地址或文本纬度/经度值。如果您将地址作为字符串传递，那么路线服务会对该字符串进行地理编码，然后将其转换为纬度/经度坐标以计算路线。如果您传递的是坐标值，请确保纬度值与经度值之间无空格。|
|sensor|用于表示路线请求是否来自装有位置传感器的设备。该值只能是 true 或 false。|

**可选参数**

|参数名|说明|
|---|---|
|mode（默认值为 driving）|用于指定计算路线时所采用的出行方式。如果您将方式指定为“公交”，那么必须同时指定 departure_time 或 arrival_time。|
|waypoints|用于指定一组路标。路标可让路线经过指定地点，从而改变路线。您可以采用纬度/经度坐标或经过地理编码的地址的形式指定路标。路标仅支持行车、步行和骑车路线。（有关路标的详情，请参阅下面的在路线中使用路标。）|
|alternatives|在设为 true 时指定路线服务可在响应中提供多条备用路线。请注意，提供备用路线可能会增加服务器的响应时间。
|avoid|用于表示计算的路线应避开指定的地图项。目前，此参数支持以下两个参数：<br>1. tolls，用于表示计算的路线应避开收费公路/桥梁。<br>2. highways，用于表示计算的路线应避开高速公路。|
|units|用于指定显示结果时所用的单位制。您可在下面的单位制中指定有效的值。|
|region|即区域代码，已指定为 ccTLD（“顶级域”）双字符值。（有关详情，请参阅下面的区域偏向。）|
|departure_time | 用于指定公交路线的期望出发时间（从世界协调时 1970 年 1 月 1 日午夜起，以秒为单位）。在请求公交路线时，必须指定 departure_time 或 arrival_time 中的一个。|
|arrival_time| 用于指定公交路线的期望到达时间（从世界协调时 1970 年 1 月 1 日午夜起，以秒为单位）。在请求公交路线时，必须指定 departure_time 或 arrival_time 中的一个。|

>arrival_time 和 departure_time 参数仅适用于公交路线。只要您请求公交路线，就必须指定这些参数中的一个。

**出行方式**

计算路线时，您可以指定要使用的交通 mode。默认情况下，将路线计算为 driving 路线。目前支持以下出行方式：

|名称|说明|
|---|---|
|driving（默认）|用于表示使用道路网络的标准行车路线。|
|walking|用于请求经过步行街和人行道（如果有的话）的步行路线。|
|bicycling|用于请求经过骑行道和优先街道（如果有的话）的骑行路线。|
|transit|用于请求经过公交路线（如果有的话）的路线。|

**示例**

标准用法

```
http://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&sensor=false
```

通过更改 mode 和 avoid 参数，您可以将初始请求修改成返回一段观光骑行之旅的路线（避开各大高速公路）。

```
http://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&sensor=false&avoid=highways&mode=bicycling
```

以下请求用于搜索从纽约布鲁克林区到纽约皇后区的公交路线。在请求公交路线时，请务必指定 departure_time 或 arrival_time。请注意，在此示例中的出发时间指定为 2012 年 7 月 30 日上午 09:45。在提交该请求前，请先将该参数更新为未来的某个时间点。

```
http://maps.googleapis.com/maps/api/directions/json?origin=Brooklyn&destination=Queens&sensor=false&departure_time=1343605500&mode=transit
```

####4、返回数据格式说明


