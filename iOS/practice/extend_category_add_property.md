#如何在为类Category时也可以添加property?
---
我们知道，可能利用`category`来扩展一个已存在的类的`方法`而不用继承一个类，这样做的好处是，看起来很爽。

可是，我们却又不能为这个类添加`property`或者`field`，因为只是为它添加扩展的`方法`而已，数据还是原类的数据。

这让我们很伤脑筋，因为很多时候我们要爽得彻底，不免为这样的约束感到无奈。

那么有什么办法可以很好地解决呢？期实是有的，那就是利用`Objective-C`的`runtime`特性。

我们利用`Objective-C`就可以轻松搞定，比方说我要为`PDConfig`类加一个`CURRENT_CITY_ID`属性，下面就是要写的代码：

**PDConfig+PDNews.h**

```
#import <Foundation/Foundation.h>
#import "PDConfig.h"

@interface PDConfig (PDNews)

@property (nonatomic, assign) int CURRENT_CITY_ID;

@end
```

**PDConfig+PDNews.m**

```
#import "PDConfig+PDNews.h"
#import <objc/runtime.h>


const static void *KEY_CURRENT_CITY_ID;

@implementation PDConfig (PDNews)

- (void)setCURRENT_CITY_ID:(int)CURRENT_CITY_ID
{
    objc_setAssociatedObject(
        self,
        &KEY_CURRENT_CITY_ID,
        @(CURRENT_CITY_ID),
        OBJC_ASSOCIATION_RETAIN_NONATOMIC
    );
}


- (int)CURRENT_CITY_ID
{
    NSNumber *number = objc_getAssociatedObject(self, &KEY_CURRENT_CITY_ID);
    return [number intValue];
}

@end
```

我们利用`objc_setAssociatedObject`函数设置与对象相关联的值，利用`objc_getAssociatedObject`获取与对象相关联的值，就像在搞一个hashMap一样。

不过觉得是稍为麻烦，就为一个属性就写了这么多代码，有没有更好的办法呢？暂时还没找到，不过这已经很好地解决上面命题的需求了。

不过`Objective-C`的`runtime`的确值得深究。