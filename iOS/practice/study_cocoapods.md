#CocoaPods学习笔记
------
##前言
一般用过Java开发J2EE应用的人都知道`maven`这个东东，它是一个软件包依赖库。而在iOS开发有没有这样的东西呢？iOS里有大量的第三方软件包，一般我们都是自己下载回来拉到自己的项目里的，有时有些库依赖较为复杂，比方说AFNetworking之类的，有没有像`maven`这样的东西来管理第三方库呢？

有的，它的名字就叫`CocoaPods`，iOS开发中强大的第三方库管理软件。

它可以轻松集成到当前的项目之中，只要你的主机安装了`CocoaPods`，那么就可以轻松下载这些第三方库，而且这些第三方库并不需要像以前那样放到版本库中管理里，而是需要时从网络下载回来用。

(-_-)发现自己非常不适合写前言，就到此打住吧。 

废话少说，来点干货。

##安装
就一个命令：

```
sudo gem install cocoapods
```

如果你是用`brew`安装的`ruby`，那么就不需要前面的`sudo`:

```
gem install cocoapods
```

##集成
###1、创建项目
`CocoaPods`是基于一个`Xcode`项目的，所以在使用`CocoaPods`之前，你得有一个已经存在的项目，这里我们建立一个叫做`PickOSC`的项目。

###2、配置
进入项目目录，这里是`PickOSC`目录，目录结构如下：

```
drwxr-xr-x  9 kut  staff  306 Jan  6 18:04 PickOSC/
drwxr-xr-x  5 kut  staff  170 Jan  6 18:04 PickOSC.xcodeproj/
drwxr-xr-x  5 kut  staff  170 Jan  6 18:04 PickOSCTests/
```

在该目录下建立一个文件名为`Podfile`的文件：

```
platform :ios, '6.0'

pod 'AFNetworking',  '1.3.3'
pod 'SVProgressHUD'
pod 'BlocksKit'
pod 'Google-Maps-iOS-SDK', '~> 1.6.1'
```

###3、这个文件的内容是怎么一回事儿呢？

首先是第一行:

```
platform :ios, '6.0'
```
这里告诉你，目标开发平台是`ios`的，iOS的版本是`6.0`。

下面几行：

```
pod 'AFNetworking',  '1.3.3'
pod 'SVProgressHUD'
pod 'BlocksKit'
pod 'Google-Maps-iOS-SDK', '~> 1.6.1'
```

`pod`命令告之`CocoaPods`这个项目依赖于哪些库，比如下面这一行：

```
pod 'AFNetworking',  '1.3.3'
```
表示依赖于`AFNetworking`库，版本号为`1.3.3`。

```
pod 'SVProgressHUD'
```
表示依赖于`SVProgressHUD`库，版本号能抓取得到的最新版本。

```
pod 'Google-Maps-iOS-SDK', '~> 1.6.1'
```
表示依赖于`Google-Maps-iOS-SDK`库，至少版本应该大于等于`1.6.1`。


###4、集成
在项目目录中执行下面命令：

```
pod install --verbose
```

然后是一堆输出：

```
Integrating client project

[!] From now on use `PickOSC.xcworkspace`.
[deprecated] I18n.enforce_available_locales will default to true in the future. If you really want to skip validation of your locale you can set I18n.enforce_available_locales = false to avoid this message.

Integrating Pod targets `Pods-AFNetworking, Pods-BlocksKit, Pods-SVProgressHUD, and Pods-libffi` into aggregate target Pods of project `PickOSC.xcodeproj`.
```

就这样，它就把依赖的第三方包集成了进来，集成后的目录结构是：

```
drwxr-xr-x   9 kut  staff   306 Jan  6 18:04 PickOSC/
drwxr-xr-x   5 kut  staff   170 Jan  6 18:04 PickOSC.xcodeproj/
drwxr-xr-x   3 kut  staff   102 Jan  6 18:18 PickOSC.xcworkspace/
drwxr-xr-x   5 kut  staff   170 Jan  6 18:04 PickOSCTests/
-rw-r--r--   1 kut  staff    87 Jan  6 18:17 Podfile
-rw-r--r--   1 kut  staff   781 Jan  6 18:18 Podfile.lock
drwxr-xr-x  33 kut  staff  1122 Jan  6 18:18 Pods/
```

###5、使用第三方库
怎么使用呢？比方说我要使用AFNetworking库？

```
#import <AFNetworking/AFHTTPClient.h>
```

很简单吧，因为已经集成，它是在`system header search path`内了，所以，你就可以直接引用它的头文件，是不是很方便？

##定制
这里先写到这儿，要下班了，等回去再说。