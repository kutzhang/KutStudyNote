#Activity Fragment 关于屏幕旋转的坑
---

> 一般情况下我们总是不会去理会屏幕旋转，我们都是竖着用手机的，当然，我们也可以把它定死在一个方向上不让它屏幕旋转。   
>   
> 但是真的就可以吗？
> 
> 答案是不，因为事实证明，当你调用启动一个屏幕方向和你现运行的应用屏幕方向不一致的Activity，当这个Activity返回的时候，这时就已经发生了屏幕旋转了。那么这时屏幕旋转所带来的所有坑你都会踩上，而且一发不可收拾。所以，在日常开发中，也是要小心处理屏幕旋转的。



## 一、屏幕旋转与Activity生命周期

### (1)机制

当发生屏幕旋转时，Android会注销当前的Activity实例，然后重建一个当前Activity类的一个实例，并启动它。在注销当前实例之前，它会调用当前实例的`protected void onSaveInstanceState(@NonNull Bundle outState)`方法，用于保存当前实例的状态。在其启动新的实例时，会调用`protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)`方法恢复上一个实例的状态到现在这个实例中。


### (2)坑

以上都是一些书籍上都会有的，但是它们都没有讲一个事情，这个新的实例的启动，等于是一个Activity的新生，它都会经历`onCreate`、`onStart`这样的生命周期，也就是说，这个实例是全新的，和之前的实例没有半毛钱关系，你之前在之前的实例上所引用的对象或者变量，都会变成`NULL`、`0`、`false`。

所以，一不小心，你就会被默明其妙的遇到个空指针异常，一头雾水地到处实例化对象，散得实例化组件的代码到处都是。更可怕的是，数据都被奇怪地置零了，一个头两个大。

### (3)埋

既然屏幕旋转后的实例是全新的，而且都会经过`onCreate`、`onStart`这样的生命周期，那么，那好了，我们就当它是一个全新启动的Activity来处理好了。

在`onCreate`中，我们初使化业务组件和UI绑定：

```
@Override
protected void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lifecycle);

    // 初始化业务组件
    this.userManager = UserManager.getInstance();
    
    // UI绑定
    this.btnSubmit = findViewById(R.id.btn_submit);
}

```

在`protected void onSaveInstanceState(@NonNull Bundle outState)`中，我们保存变量：

```
@Override
protected void onSaveInstanceState(@NonNull Bundle outState)
{
    super.onSaveInstanceState(outState);
    Log.d(Constants.DEBUG_LABEL, "on activity save state");

    outState.putString(SKEY_NAME, etName.getText().toString());
    outState.putString(SKEY_COMMENT, etComment.getText().toString());
}
```

在`protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)`中，我们恢复变量：

```
@Override
protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)
{
    super.onRestoreInstanceState(savedInstanceState);

    etName.setText(savedInstanceState.getString(SKEY_NAME));
    etComment.setText(savedInstanceState.getString(SKEY_COMMENT));
}
```

这样，我们就埋了Activity在屏幕旋转后所挖出来的坑。

## 二、屏幕旋转与Fragment

### (1)机制

在`生命周期`上，Fragment和Activity是类似的，所以这里不再详述。

### (2)坑与埋

但是在某些场景上，由于Fragment与Activity的从属关系，在Activity初始化Fragment的一些属性时会因为屏幕旋转而产生崩溃问题，如下面这段Activity代码：

```
@Override
protected void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dialog);

    FragmentManager fragmentManager = getFragmentManager();
    detailFragment = new DialogDetailFragment();
    detailFragment.setDelegate(this);
    fragmentManager.beginTransaction()
            .add(R.id.fragement_container, detailFragment)
            .commit();
}

```

上面这些代码会因为Fragment已经加入过Activity的FragmentManager而导致异常。这是为什么呢？不是说是全新的Activity实例吗？为什么说Fragment已经添加过了呢？

主要是这样，在旋转时，Android会把当前加入到FragmentManager的Fragment加入到队列中，但只是记录了Fragment类的信息，在旋转后，会根据这些Fragment类信息，Android会重新创建Fragment，并加入到FragmentManager中，所以也就有了之前的代码会产生异常的情况。

> 由上述也可以明白，为什么Fragment会有`setArguments`这个方法，而不是直接使用构造方法了。因为需要一个无参构造方法来从Fragment队列中恢复实例，所以只能这么干了。

那么上述代码按理就可以改为：

```
@Override
protected void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dialog);

    FragmentManager fragmentManager = getFragmentManager();
    DialogDetailFragment detailFragment =
            (DialogDetailFragment) fragmentManager.findFragmentById(R.id.fragement_container);
    if (detailFragment == null)
    {
        detailFragment = new DialogDetailFragment();
        detailFragment.setDelegate(this);
        fragmentManager.beginTransaction()
                .add(R.id.fragement_container, detailFragment)
                .commit();
    }
}
```

同样，这样是解决了重复添加的问题，但是，却引发了另一个问题，导致`NullPointerException`。为什么呢？从代码可以知道，当`detailFragment`为空时才设置`delegate`属性，这种是最初Activity创建时的状态，那么在旋转后呢？这个`detailFragment`不为空，那么这时的`detailFragment`是由Android自行实例化的，也就是说是一个全新的Fragment实例，这时的`detailFragment`的`delegate`属性并没有赋值，所以当`detailFragment`在使用到`delegate`属性时会导致`NullPointerException`。

那么怎么办呢？既然每次进入`onCreate`都是全新的Activity，那么就每次都给相应的Fragment初始化好了（除了创建并添加Fragment）：

```
@Override
protected void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dialog);

    FragmentManager fragmentManager = getFragmentManager();
    DialogDetailFragment detailFragment =
            (DialogDetailFragment) fragmentManager.findFragmentById(R.id.fragement_container);
    if (detailFragment == null)
    {
        detailFragment = new DialogDetailFragment();
        fragmentManager.beginTransaction()
                .add(R.id.fragement_container, detailFragment)
                .commit();
    }
    // fixed
    detailFragment.setDelegate(this);
}
``` 

## 三、最佳实践

OK，这样一切都清晰了。

以前，一般情况下，我们开发应用都不用理会屏幕旋转，而且我也是这样想的。但是，在开发过程中，也会因为一些特殊情况导致莫明其妙的崩溃异常，所以，最佳实践是，每次编写Activity的时候，都留心屏幕旋转或者应用转入后台的数据状态保存与恢复。这样的话，会尽可能得减少应用因Activity生命周期的变化导致崩溃异常。

还是那句话，多写几行预防崩溃的代码，会减少你调试代码的时间。